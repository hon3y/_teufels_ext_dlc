<?php

defined('TYPO3_MODE') or die();

$tca_pages = [
    'not_for_downloadcenter' => [
        'exclude' => 1,
        'label' => 'LLL:EXT:teufels_ext_dlc/Resources/Private/Language/translation_db.xlf:pages.not_for_downloadcenter',
        'config' => [
            'type' => 'check'
        ],
    ],
];

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns(
    'pages',
    $tca_pages
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addFieldsToPalette(
    'pages',
    'visibility',
    'not_for_downloadcenter',
    'after:nav_hide'
);