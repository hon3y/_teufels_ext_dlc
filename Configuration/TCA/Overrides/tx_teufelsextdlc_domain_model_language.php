<?php

defined('TYPO3_MODE') or die();

$sModel = 'tx_teufelsextdlc_domain_model_language';

$GLOBALS['TCA'][$sModel]['ctrl']['label'] = 'title';
#$GLOBALS['TCA'][$sModel]['ctrl']['label_alt'] = 'iso6391, iso31661';
#$GLOBALS['TCA'][$sModel]['ctrl']['label_alt_force'] =  1;

$GLOBALS['TCA'][$sModel]['ctrl']['default_sortby'] = 'ORDER BY title ASC';