##
## http://blog.systemfehler.net/erweiterung-des-typo3-kategoriensystems/
##
#config.tx_extbase
plugin.tx_teufelsextdlc {
  persistence {
    classes{
      TEUFELS\TeufelsExtDlc\Domain\Model\Category {
        mapping {
          tableName = sys_category
          columns {

          }
        }
      }
    }
  }
}