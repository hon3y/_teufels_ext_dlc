<?php
namespace TEUFELS\TeufelsExtDlc\Domain\Repository;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2017 Andreas Hafner <a.hafner@teufels.com>, teufels GmbH
 *           Dominik Hilser <d.hilser@teufels.com>, teufels GmbH
 *           Georg Kathan <g.kathan@teufels.com>, teufels GmbH
 *           Josymar Escalona Rodriguez <j.rodriguez@teufels.com>, teufels GmbH
 *           Hendrik Krüger <h.krueger@teufels.com>, teufels GmbH
 *           Timo Bittner <t.bittner@teufels.com>, teufels GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * The repository for Pages
 */
class PageRepository extends \TYPO3\CMS\Frontend\Page\PageRepository
{
    /**
     * @param $pid
     * @param array $aSettings
     * @return array
     */
    public function findByPid($sPidList = '0', $aSettings = array(), $bShowParentSiteInMenu = false) {

        $sDoktypes = $aSettings['production']['api']['query']['pages']['doktypes'];
        $sAndNavHideIn = $aSettings['production']['api']['query']['pages']['andNavHideIn'] != "" ? $aSettings['production']['api']['query']['pages']['andNavHideIn'] : 0;

        $aResult = array();
        if ($sPidList != "" && $sDoktypes != "") {
            $aPid = \TYPO3\CMS\Core\Utility\GeneralUtility::intExplode(',', $sPidList);
            foreach ($aPid as $iPid) {
                $sFields = 'uid,pid,doktype,title,nav_title';
                $sSortField = 'sorting';

                $aPage = [];
                if (count($aPid) > 1) {
                    $aPage = $this->getPage($iPid);
                }

                $aSubPages = $this->getMenu($iPid, $sFields, $sSortField, " AND pages.nav_hide IN ($sAndNavHideIn) AND pages.hidden IN (0) AND pages.not_for_downloadcenter IN (0) AND pages.doktype IN ($sDoktypes)");

                if (count($aPage) > 0) {
//                    if (count($aSubPages) > 0) {
//                        $i = 0;
//                        foreach ($aSubPages as $key => $value) {
//                            $aSubPages[$key]["title"] =
//                                ($i == count($aSubPages)-1 ? "└─ " . $aSubPages[$key]["title"] : "├─ " . $aSubPages[$key]["title"]);
//                            $aSubPages[$key]["nav_title"] =
//                                ($aSubPages[$key]["nav_title"] != "" ?
//                                    ($i == count($aSubPages)-1 ? "└─ " . $aSubPages[$key]["nav_title"] : "├─ " . $aSubPages[$key]["nav_title"]) : "");
//                            $i++;
//                        }
//                    }
                    array_unshift(
                        $aSubPages,
                        [
                            "uid" => $aPage["uid"],
                            "pid" => $aPage["pid"],
                            "title" =>$aPage["title"],
                            "nav_title" => ($aPage["nav_title"] != "" ? $aPage["nav_title"] : ""),
                            "optgroup" => 1
                        ]
                    );
                }
                if (count($aSubPages) > 0) $aResult[] = array_values($aSubPages);
            }
        }

        return $aResult;
    }

    public function findByPidListAndLevels ($sPidList = '0', $iLevels = 1, $aSettings = array()) {

        $sDoktypes = $aSettings['production']['api']['query']['pages']['doktypes'];
        $sAndNavHideIn = $aSettings['production']['api']['query']['pages']['andNavHideIn'] != "" ? $aSettings['production']['api']['query']['pages']['andNavHideIn'] : 0;
        $sAnd = " AND pages.nav_hide IN ($sAndNavHideIn) AND pages.hidden IN (0) AND pages.not_for_downloadcenter IN (0) AND pages.doktype IN ($sDoktypes)";

        $aResult = array();
        if ($sPidList != "" && $sDoktypes != "") {
            $aPid = \TYPO3\CMS\Core\Utility\GeneralUtility::intExplode(',', $sPidList);
            foreach ($aPid as $iPid) {
                $sFields = 'uid,pid,doktype,title,nav_title';
                $sSortField = 'sorting';
                $aSubPages = $this->getMenu($iPid, $sFields, $sSortField, $sAnd);
                if (count($aSubPages) > 0) {
                    if($iLevels >= 2) {
                        foreach ($aSubPages as &$aSubPage) {
                            $aSubPages_ = $this->getMenu($aSubPage['uid'], $sFields, $sSortField, $sAnd);
                            if (count($aSubPages_) > 0) {
                                if($iLevels >= 3) {
                                    foreach ($aSubPages_ as &$aSubPage_) {
                                        $aSubPages__ = $this->getMenu($aSubPage_['uid'], $sFields, $sSortField, $sAnd);
                                        if (count($aSubPages_) > 0) {
                                            if($iLevels >= 4) {
                                                foreach ($aSubPages__ as &$aSubPage__) {
                                                    $aSubPages___ = $this->getMenu($aSubPage__['uid'], $sFields, $sSortField, $sAnd);
                                                    if (count($aSubPages___) > 0) {
                                                        $aSubPage__['subpages'] = array_values($aSubPages___);
                                                    }
                                                }
                                            }
                                            $aSubPage_['subpages'] = array_values($aSubPages__);
                                        }
                                    }
                                }
                                $aSubPage['subpages'] = array_values($aSubPages_);
                            }
                        }
                    }
                    $aResult[] = array_values($aSubPages);
                }
            }
        }

        return $aResult;

    }

}