<?php
namespace TEUFELS\TeufelsExtDlc\Domain\Repository;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2017 Andreas Hafner <a.hafner@teufels.com>, teufels GmbH
 *           Dominik Hilser <d.hilser@teufels.com>, teufels GmbH
 *           Georg Kathan <g.kathan@teufels.com>, teufels GmbH
 *           Josymar Escalona Rodriguez <j.rodriguez@teufels.com>, teufels GmbH
 *           Hendrik Krüger <h.krueger@teufels.com>, teufels GmbH
 *           Timo Bittner <t.bittner@teufels.com>, teufels GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * The repository for Categories
 * http://blog.systemfehler.net/erweiterung-des-typo3-kategoriensystems/
 */
class CategoryRepository extends \TYPO3\CMS\Extbase\Domain\Repository\CategoryRepository
{

    protected $defaultOrderings = array(
        'sorting' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING,
        'title' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING
    );


    /**
     * @param $parent
     * @param array $categories2skip
     * @return array
     */
    public function findByParent($iParent)
    {
        $query = $this->createQuery();
        $constraints = array();
        $constraints[] = $query->equals('parent', $iParent);
        $query->matching($query->logicalAnd($constraints));
        $result = $query->execute()->toArray();
        return $result;
    }

    /**
     * @param $parent
     * @param array $categories2skip
     * @return array
     */
    public function findByParents($iParent, $sSkipCategories = '')
    {
        $aResult = array();

        $aLevel1 = $this->findByParent($iParent);
        foreach ($aLevel1 as $oCategory) {
            $aResult[] = array(
                $oCategory,
                $this->findByParent($oCategory->getUid())
            );
        }

        $aSkipCategories = explode(",", $sSkipCategories);

        foreach ($aResult as $k1 => $l1) {
            //var_dump($l1[0]->getUid());
            if (in_array($l1[0]->getUid(), $aSkipCategories)) {
                unset($aResult[$k1]);
            }
            if (count($l1[1]) > 0) {
                foreach ($l1[1] as $k2 => $l2) {
                    //var_dump($l2->getUid());
                    if (in_array($l2->getUid(), $aSkipCategories)) {
                        unset($aResult[$k1][$k2]);
                    }
                }
            }

        }

        return $aResult;
    }

}